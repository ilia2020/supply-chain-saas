# Intro

SAAS solution for supply chain problems.


# Up server manually
Activate virtual environment
```
venv\Scripts\activate
```
Setup environment variables

Run migrations
```
python manage.py migrate
```
Start server
```
python manage.py runserver
```