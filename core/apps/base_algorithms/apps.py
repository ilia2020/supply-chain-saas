from django.apps import AppConfig


class BasicAlgorithmsConfig(AppConfig):
    name = 'apps.base_algorithms'
