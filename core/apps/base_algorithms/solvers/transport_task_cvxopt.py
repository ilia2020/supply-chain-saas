"""
Use http://cvxopt.org/install/index.html to solve transport tasks.

Examples:
https://sudonull.com/post/68110-Solving-a-closed-transport-problem-with-additional-conditions-using-Python

CVXR docs
https://cran.r-project.org/web/packages/CVXR/CVXR.pdf

CVXPY
https://www.cvxpy.org/

CVXOPT
https://readthedocs.org/projects/cvxopt/downloads/pdf/latest/

Transport Problem example with CVXOPT page 4
http://www.acme.byu.edu/wp-content/uploads/2014/09/Vol2Lab14.pdf
"""

from cvxopt.modeling import sum, matrix, solvers
from functools import reduce

import numpy as np


class TransportTaskCVXOPTSolver:
    """
    Solver for balanced supply chain tasks.

    Assume that we are the manager of a support chain.
    Our company has two factory A and B, and each of them has 300 and 500 products, respectively.
    store1 needs 200
    store2 needs 300
    store3 needs 250
    Now we would like to deliver them two three retail stores in different cities 1, 2, 3.
    There is a cost for each delivery as listed below.
                St1 st2	St3
    Factory A 	5 	6 	4
    Factory B 	6 	3 	7

    https://www.xiaowenying.com/machine-learning/2019/11/12/transportation-problem.html

    # from cvxopt import matrix, solvers
    # cost matrix
    c = matrix([5., 6., 4., 6., 3., 7.])

    G = matrix([[1., 0., -1., 0., 0., 0., 0., 0.],
                [1., 0., 0., -1., 0., 0., 0., 0.],
                [1., 0., 0., 0., -1., 0., 0., 0.],
                [0., 1., 0., 0., 0., -1., 0., 0.],
                [0., 1., 0., 0., 0., 0., -1., 0.],
                [0., 1., 0., 0., 0., 0., 0., -1.], ])
    h = matrix([300., 500., 0., 0., 0., 0., 0., 0.])
    A = matrix([[1., 0., 0.],
                [0., 1., 0.],
                [0., 0., 1.],
                [1., 0., 0.],
                [0., 1., 0.],
                [0., 0., 1.], ])
    b = matrix([200., 300., 250.])
    sol = solvers.lp(c, G, h, A, b)
    print(sol['x'])
    for i in range(6):
        print('x{}={}'.format(i + 1, round(sol['x'][i])))

    :return:
    """

    def __init__(
            self,
            cost_np_array: np.array,
            rows_supply_label: list,
            columns_demand_label: list,
            limits_supply: dict,
            limits_demand: dict
    ):
        """
        https://stackoverflow.com/questions/5048299/how-do-i-print-an-aligned-numpy-array-with-text-row-and-column-labels
        :param price_np_array:
        """
        self.cost_np_array = cost_np_array
        self.rows_count, self.columns_count = self.cost_np_array.shape
        self.rows_supply_label = rows_supply_label
        self.columns_demand_label = columns_demand_label
        self.limits_supply = limits_supply
        self.limits_demand = limits_demand
        self._c = self._init_cost_matrix()
        self._G = self._init_matrix_G()
        self._h = self._init_matrix_of_supply_limits()
        self._A = self._init_matrix_A()  #should be correct
        self._b = self._init_matrix_of_demand_limits()

        self._solution = None

    def _init_cost_matrix(self):
        # return matrix(list(chain(*self.cost_np_array.tolist())))
        l1 = reduce(lambda x, y: x+y, self.cost_np_array.tolist())
        l2 = [float(x) for x in l1]
        return matrix(l2)

    @property
    def cost_size(self):
        return self.cost_np_array.size

    @property
    def demand_count(self):
        return len(self.columns_demand_label)

    @property
    def supply_count(self):
        return len(self.rows_supply_label)

    def _init_matrix_of_supply_limits(self):
        result = [float(self.limits_supply[supplier]) for supplier in self.rows_supply_label]
        appendix = [0. for i in range(self.cost_size)]
        return matrix(result+appendix)

    def _init_matrix_of_demand_limits(self):
        return matrix([float(self.limits_demand[demand]) for demand in self.columns_demand_label])

    def _init_matrix_A(self):
        base = []
        for i in range(self.demand_count):
            base_list = [0. for i in range(self.demand_count)]
            base_list[i] = 1.
            base.append(base_list)
        base = base * self.supply_count
        return matrix(base)

    def _init_matrix_G(self):
        result = []
        base_array_size = self.supply_count+self.demand_count*self.supply_count
        for supplier_idx in range(self.supply_count):
            for demand_idx in range(self.demand_count):
                base_array = [0. for i in range(base_array_size)]
                base_array[supplier_idx] = 1.
                base_array[self.supply_count+supplier_idx*self.demand_count+demand_idx] = -1.
                result.append(base_array)
        return matrix(result)

    def __repr__(self):
        return f"Cost np array: {self.cost_np_array}"

    def solve(self):
        self._solution = solvers.lp(self._c, self._G, self._h, self._A, self._b)
        return self._solution['x']

    @property
    def total_cost(self):
        total_cost_matrix = self._c.T * self._solution['x']
        return sum(total_cost_matrix)

    def print_result(self):
        print(self._solution['x'])
        for i in range(len(self._solution['x'])):
            print('x{}={}'.format(i + 1, round(self._solution['x'][i], 2)))

        print(f'total cost: {round(self.total_cost, 2)}')
