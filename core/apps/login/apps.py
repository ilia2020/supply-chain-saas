from django.apps import AppConfig


class SocialLoginConfig(AppConfig):
    name = 'apps.login'
