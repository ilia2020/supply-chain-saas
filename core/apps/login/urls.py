from django.urls import path, include
from dj_rest_auth.registration.views import VerifyEmailView

from .views import GoogleLogin

urlpatterns = [
    # path('', include('django.contrib.auth.urls')),
    path('dj-rest-auth/', include('dj_rest_auth.urls')),
    path('dj-rest-auth/registration/', include('dj_rest_auth.registration.urls')),
    path('dj-rest-auth/google/$', GoogleLogin.as_view(), name='google_login'),
    path('dj-rest-auth/account-confirm-email/', VerifyEmailView.as_view(), name='account_email_verification_sent'),
]