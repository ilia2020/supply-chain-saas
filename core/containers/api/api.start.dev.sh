#!/usr/bin/env bash
#./project/scripts/wait-for-it.sh database:3306 -s -t 20

echo /backend/source/
cd /backend/source/
ls

python /backend/source/manage.py makemigrations --settings=project.settings || { echo 'makemigrations has been failed' ; exit 1; }
python /backend/source/manage.py migrate  --settings=project.settings --database=dev || { echo 'migration has been failed' ; exit 1; }
#python /backend/core/project/manage.py migrate --database=client1 --settings=project.settings.development || { echo 'migration has been failed' ; exit 1; }
#python /backend/core/project/manage.py migrate --database=client2 --settings=project.settings.development || { echo 'migration has been failed' ; exit 1; }

python /backend/source/manage.py collectstatic --settings=project.settings --no-input || { echo 'collectstatic has been failed' ; exit 1; }
#uwsgi --ini /backend/core/project/config/uwsgi/uwsgi.ini || { echo 'UWSGI server failed to start' ; exit 1; }
python /backend/source/manage.py runserver 0.0.0.0:8000
