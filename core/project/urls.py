"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin

from allauth.account.views import ConfirmEmailView
from django.urls import path, include

from apps.login.views import empty_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path(
            "api/v0/",
            include(
                (
                    [
                        path("", include("apps.login.urls")),
                    ],
                    "apps"
                ),
                namespace="v0",
            ),
        ),
    path('account-confirm-email/(?P<key>[-:\w]+)/$', ConfirmEmailView.as_view(), name='account_confirm_email'),
    path('password-reset/<uidb64>/<token>/', empty_view, name='password_reset_confirm'),
    # TODO (remove accounts, rewrite account-confirm-email endpoint)
    path('accounts/', include('allauth.urls')),
]
